<!-- begin:: Content -->
                        <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

                           <div class="row">
                               <div class="col-md-4">
                                   <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
										
										<div class="kt-portlet__body">
											<div class="kt-profile-pic">
												<img src="<?=base_url()?>assets/media/users/100_1.jpg" style='height:auto;width: 40%;'>
											</div>
											<h5 style="text-align: center;margin-top: 20px"><b>Name</b></h5>
											<span style="text-align: center;">Role</span>											
										</div>
									</div>
                               </div>
                               <div class="col-md-8">
                               		<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
										<div class="kt-portlet__body">
											<ul class="nav nav-tabs  nav-tabs-line" role="tablist">
												<li class="nav-item">
													<a class="nav-link active" data-toggle="tab" href="#kt_tabs_1_1" role="tab">Personal Info</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#kt_tabs_1_2" role="tab">Change Avatar</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#kt_tabs_1_3" role="tab">Change Password</a>
												</li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel" style="padding: 10px">
													<form role="form" action="#">
                                                                <div class="form-group">
                                                                    <label class="control-label">Name</label>
                                                                    <input type="text" placeholder="John" class="form-control"> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Email</label>
                                                                    <input type="text" placeholder="Doe" class="form-control"> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Mobile Number</label>
                                                                    <input type="text" placeholder="+1 646 580 DEMO (6284)" class="form-control"> </div>
                                                                <div class="form-group" class="">
                                                                	<button class="btn btn-primary pull-right" style="margin: 5px"><i class="fa fa-save"></i> Save</button>
                                                                	<button class="btn btn-danger pull-right" style="margin: 5px"><i class="fa fa-window-close"></i> Cancel</button>
                                                                </div>
                                                                
                                                            </form>
												</div>
												<div class="tab-pane" id="kt_tabs_1_2" role="tabpanel" style="padding: 10px">
													<form action="#" role="form">
                                                                <div class="form-group">
                                                                    <div class="fileinput fileinput-exists" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                                                        
                                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                                <span class="fileinput-new"> Select image </span>
                                                                                <span class="fileinput-exists"> Change </span>
                                                                                <input type="hidden" value="" name=""><input type="file" name="..."> </span>
                                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix margin-top-10">
                                                                        <span class="label label-danger">NOTE! </span>
                                                                        <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                                    </div>
                                                                </div>
                                                                <div class="margin-top-10">
                                                                    <a href="javascript:;" class="btn green"> Submit </a>
                                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                                </div>
                                                            </form>
												</div>
												<div class="tab-pane" id="kt_tabs_1_3" role="tabpanel" style="padding: 10px">
                                                            <form action="#">
                                                                <div class="form-group">
                                                                    <label class="control-label">Current Password</label>
                                                                    <input type="password" class="form-control"> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">New Password</label>
                                                                    <input type="password" class="form-control"> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Re-type New Password</label>
                                                                    <input type="password" class="form-control"> </div>
                                                                <div class="margin-top-10">
                                                                    <button class="btn btn-primary pull-right" style="margin: 5px"><i class="fa fa-save"></i> Save</button>
                                                                	<button class="btn btn-danger pull-right" style="margin: 5px"><i class="fa fa-window-close"></i> Cancel</button>
                                                                </div>
                                                            </form>
                                                </div>
											</div>
										</div>
									</div>
                               </div>
                           </div>
                        </div>

                    
